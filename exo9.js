/*algo ListeDachatEtPayment

var sommeDue = reel
var sommePayee = reel
var rendu = reel
var nombre = reel
var remise = reel
var dix = reel
var cinq = reel
var un = reel
var memoireremise = reel

debut
sommeDue = 0
sommePayee = 0
nombre = 1
remise = 0
dix = 0
cinq = 0
un = 0
  tant que nombre n'est pas egal a 0 
    afficher("saisir un nombre (non nul), saisir 0 pour arreter la saisie")
    saisir(nombre)
    sommeDue = sommeDue + nombre
  ftq
  afficher("vous devez : /sommeDue/ euros, indiquez le montant que vous payez :")
  saisir(sommePayee)
  remise = SommePayee - sommeDue
  memoireremise = remise
  tant que remise > 10 
    remise -= 10
    dix++
  ftq
  tant que remise > 5
    remise -= 5
    cinq++
  ftq
  tant que remise > 0 
    remise -= 1
    un++
  ftq
  afficher ("votre remise est de /memoireremise/ vous recevez /dix/ billet(s) de 10e, /cinq/ billet(s) de 5e, /un/ piece(s) de 1e")
*/
//code

function exo(){
    let sommeDue = 0
    let sommePayee = 0
    let nombre = 1
    let remise = 0
    let dix = 0
    let cinq = 0
    let un = 0
    
    while (nombre !== 0){
        nombre = Number(prompt("saisir un nombre (non nul), saisir 0 pour arreter la saisie"))
        sommeDue+=nombre  
      }
    alert("vous devez : "+sommeDue+" euros")
    sommePayee = Number(prompt("indiquez le montant que vous payez : "))
    remise = sommePayee - sommeDue
    let remiseinit = remise
    while (remise > 10){
      remise-=10
      dix++
    }
    while (remise > 5){
      remise-=5
      cinq++
    }
    while (remise > 0){
      remise-=1
      un++
    }
  return "votre remise est de : "+remiseinit+" euros, vous recevez :\n"+dix+" billet(s) de 10e\n"+cinq+" billet(s) de 5e\n"+un+"piece(s) de 1e"
  }
  
  const result = exo();
  console.log(result);