/*algo SaisirRandomTableauRetournePlusGrandNombre

var nombre = reel
var compteur = reel
var tableau = []

debut 
  afficher ("saisir un nombre (non nul), saisir 0 pour arreter la saisie")
  nombre=1
  compteur = 0
  tant que nombre n'est pas egal a  0
    saisir (nombre)
    tableau[compteur]=nombre
    compteur++
  ftq
 afficher la plus grande valeur du tableau et son index
fin
*/
//code

function exo(){
    let tableau = []
    let compteur = 0
    let nombre = 1
    while (nombre !== 0){
      nombre = Number(prompt("saisir un nombre (non nul), saisir 0 pour arreter la saisie"))
      tableau[compteur] = nombre
      compteur++
    }
    return "le plus grand nombre saisit est: "+Math.max(...tableau)+" en position "+(tableau.indexOf(Math.max(...tableau))+1)
  }
  
  const result = exo();
  console.log(result);