/*algo DemandeUnNombreAlutilisateur1-3

var nombre:reel

debut
    afficher ("saisir un nombre")
    saisir (nombre)
    tant que nombre > 3 ou nombre < 1
        afficher ("saisir un autre nombre")
        saisir(nombre)
    ftq
    afficher (nombre)
fin

*/
//code
function exo() {
    let nombre = prompt("saisir un nombre")
    while (nombre > 3 || nombre < 1) {
        nombre = prompt("saisir un autre nombre")
    }
    return nombre
}

const result = exo();
console.log(result);