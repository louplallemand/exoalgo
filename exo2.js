/*algo DemandeUnNombreAlutilisateur10-20

var nombre: reel

debut
  afficher ("saisir un nombre")
  saisir(nombre)
  tant que 10<nombre<20
    si nombre > 20
      alors afficher ("saisir un autre nombre")
            saisir(nombre)
    sinon si nombre < 10
      alors afficher ("saisir un autre nombre")
            saisir (nombre)
    fsi
  ftq
 afficher nombre
            
*/
//code

function exo(){
    let nombre = Number(prompt("saisir un nombre"))
    while (nombre<10 || nombre>20){
      if (nombre<10){
        nombre = Number(prompt("plus grand"))
      }else if (nombre>20){
        nombre = Number(prompt("plus petit"))
      }
    }
    return nombre
  }
  
  const result = exo();
  console.log(result);